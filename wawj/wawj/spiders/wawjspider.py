# -*- coding: utf-8 -*-
import scrapy
from wawj.items import wawjItem
from scrapy.http import Request

import json
import pandas
import pymysql.cursors

class wawjSpider(scrapy.Spider):
	name = 'wawjspider'
	start_urls = ["https://sh.5i5j.com/ershoufang/"]#,"https://bj.5i5j.com/ershoufang/"]
	base_url = 'http://sh.5i5j.com'

	def parse(self, response):
		base_url = response.url+'/../'
		# with open('/Users/Steven/Desktop/debug/charm/real-estate/wawj/test.json','wb') as f:
			# f.write(response.body)
		# city_url_list = response.xpath('//div[@class="top-city-menu clear"]/ul/li/p/a/@href]').extract()[0]
		maxnum = int(response.xpath('//div[@class="total-box noBor"]/span/text()').extract()[0])
		if maxnum % 30 == 0:
			maxpage = maxnum//30
		else:
			maxpage = maxnum//30 + 1

		#maxpage+1

		for page in range(1,maxpage+1):#30):# 
			url = response.url + 'n' + str(page)
			yield Request(url,callback=self.get_sourceid)
    
	def get_sourceid(self,response):
        # a_list = response.xpath('//div[@class="listCon"]/h3/a')
		house_list = response.xpath('//div[@class="listCon"]')
        # for a in a_list:
		for a in house_list:
			item = wawjItem()
			title = a.xpath('./h3/a/text()').extract()[0]
			item['title'] = title
			houseinfo = a.xpath('./div[@class="listX"]/p[position()=1]/text()').extract()[0]
			item['houseinfo'] = houseinfo
			tmp = houseinfo.split('·')
			item['roomtype'] = tmp[0]
			item['size'] = float(tmp[1].split(' ')[0])
			item['oriented'] = tmp[2].split(' ')[2]
			item['floor'] = tmp[3].split(' ')[2]
			item['decoration'] = '简装'
			if len(tmp)==5:
				item['decoration'] = tmp[4].split(' ')[2]

			unitprice = float(a.xpath('./div[@class="listX"]/div/p[position()=2]/text()').extract()[0].split('价')[1].split('元')[0])
			item['unitprice'] = unitprice
			href = a.xpath('./h3/a/@href').extract()[0]
			srcid = href.split('/')[2]
			src_url = self.base_url + href
			yield Request(src_url,callback=self.get_info,dont_filter=True,meta={'item':item})

	def get_info(self,response):
		print('second-hand house source url: ',response.url)
		item = response.meta['item']
		node = response.xpath('//div[@class="housesty"]/div[class="jlyoubai fl"]/p[1]')  

		longitude = 'None'
		latitude = 'None'
		province = '上海市'
		city = '上海市'
		district = 'None'
		street = 'None' #shangquan
		# roomtype = 'None'
		age = 'None'
		elevator = 'None'
		# floor = 'None'
		# size = 'None'
		# oriented = 'None'
		price = 'None'
		# unitprice = 'None'
		decoration = '简装'
		datasource = 2 
		
        # 可简化
		position_tmp = response.xpath('//script[contains(text(),"community_x")]/text()').extract()[0]
		longitude = float(position_tmp.split('var')[16].split(' = ')[1].split('"')[1])
		latitude =  float(position_tmp.split('var')[17].split(' = ')[1].split('"')[1])
		
		district = response.xpath('//div[@class="cur-path-box"]/div/div/a[position()=3]/text()').extract()[0].split('二手房')[0]
		
		street = response.xpath('//div[@class="infomain fl"]/ul/li[position()=4]/text()').extract()[0]
		
		# roomtype = response.xpath('//div[@class="housesty"]/div[position()=3]/div/p[position()=1]/text()').extract()[0]
		# roomtype = response.xpath('//div[@class="infocon fl"]/ul/li[position()=1]/span/text()').extract()[0]
		
		age = int(response.xpath('//div[@class="infomain fl"]/ul/li[position()=2]/text()').extract()[0].split('年')[0])
		elevator = '无'
		
		# floor = response.xpath('//div[@class="zushous"]/ul/li[2]/text()').extract()[0]
		# size = float(response.xpath('//div[@class="housesty"]/div[position()=4]/div/p[position()=1]/text()').extract()[0])
		# oriented = response.xpath('//div[@class="zushous"]/ul/li[3]/text()').extract()[0]
		# price = float(response.xpath('//div[@class="jlquannei fontbaise"]/p[@class="jlinfo"]/text()').extract()[0])
		price = item['size']*item['unitprice']
		# unitprice = float(response.xpath('//div[@class="housesty"]/div[position()=2]/div/p[position()=1]/text()').extract()[0])
        # 简化
		# decoration = response.xpath('//div[@class="zushous"]/ul/li[4]/text()').extract()[0]
		
		datasource = 2 

		item['longitude'] = longitude
		item['latitude'] = latitude
		item['province'] = province
		item['city'] = city
		item['district'] = district
		item['street'] = street
		# item['roomtype'] = roomtype 

		item['age'] = age
		item['elevator'] = elevator

		# item['floor'] = floor
		# item['size'] = size
		# item['oriented'] = oriented
		item['price'] =  price
		# item['unitprice'] = unitprice
		
		# item['decoration'] = decoration
		item['datasource'] = datasource
		
		if longitude-latitude>20 :#and age>1950:
			self.insert_record(item)
		yield item
        

	def insert_record(self,item):
		conn = pymysql.connect(host='sh-cdb-jg6kijlv.sql.tencentcdb.com', 
								port=63259, 
								user='root', 
								password='Tiankong1234', 
								db='fang',
								charset='utf8mb4', 
								cursorclass=pymysql.cursors.DictCursor)
		
		try:
			with conn.cursor() as cursor:
				# sql = """INSERT INTO wawj_zzp(title,longitude,latitude,city,roomtype,floor,size,oriented,price,unitprice,decoration,datasource)
				# VALUES('%s',%f,%f,'%s','%s','%s',%f,'%s',%f,%f,'%s',%d)"""
				sql = "INSERT INTO wawj_zzp(title,longitude,latitude,province,city,district,street,room_type,age,elevator,floor,size,oriented,price,unit_price,decoration,data_source)VALUES('"+\
				item['title']+"',"+str(item['longitude'])+","+str(item['latitude'])+",'"+item['province']+"','"+item['city']+"','"+item['district']+"','"+item['street']+"','"+item['roomtype']+"',"+\
				str(item['age'])+",'"+item['elevator']+"','"+item['floor']+"',"+str(item['size'])+",'"+item['oriented']+"',"\
				+str(item['price'])+","+str(item['unitprice'])+",'"+item['decoration']+"',"+str(item['datasource'])+")" 
				# sql = "INSERT INTO wawj_zzp(title,longitude,latitude,city,roomtype,floor,size,oriented,price,unitprice,decoration,datasource)\
				# VALUES('%s',%f,%f,'%s','%s','%s',%f,'%s',%f,%f,'%s',%d)"
				# VALUES('%s','%f','%f','%s','%s','%s','%f','%s','%f','%f','%s','%d')
				cursor.execute(sql)
				# ,("'"+item["title"]+"'",item['longitude'],item['latitude'],'sh',"item['roomtype']","item['floor']",item['size'],"item['oriented']",
					# item['price'],item['unitprice'],"item['decoration']",item['datasource'])
			conn.commit()
		finally:
			conn.close()

#    start_urls = ['http://hz.5i5j.com/community/p7/']
'''   start_urls =['https://sh.5i5j.com/xiaoqu/pudongxinqu/n1',
                     'https://sh.5i5j.com/xiaoqu/minxingqu/n1',
                     'https://sh.5i5j.com/xiaoqu/xuhuiqu/n1',
                     'https://sh.5i5j.com/xiaoqu/putuoqu/n1',
                     'https://sh.5i5j.com/xiaoqu/baoshanqu/n1',
                     'https://sh.5i5j.com/xiaoqu/changningqu/n1',
                     'https://sh.5i5j.com/xiaoqu/yangpuqu/n1',
                     'https://sh.5i5j.com/xiaoqu/songjiangqu/n1',
                     'https://sh.5i5j.com/xiaoqu/hongkouqu/n1',
                     'https://sh.5i5j.com/xiaoqu/jiadingqu/n1',
                     'https://sh.5i5j.com/xiaoqu/huangpuqu/n1',
                     'https://sh.5i5j.com/xiaoqu/jinganqu/n1',
                     'https://sh.5i5j.com/xiaoqu/qingpuqu/n1',
                     'https://sh.5i5j.com/xiaoqu/n2/',
                     'https://sh.5i5j.com/xiaoqu/n3/',
                     'https://sh.5i5j.com/xiaoqu/n4/',
                     'https://sh.5i5j.com/xiaoqu/n5/',
                     'https://sh.5i5j.com/xiaoqu/n6/',
                     'https://sh.5i5j.com/xiaoqu/n7/']
'''