# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class wawjItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    title = scrapy.Field()
    longitude = scrapy.Field()
    latitude = scrapy.Field()
    province = scrapy.Field()
    city = scrapy.Field()
    district = scrapy.Field()
    street = scrapy.Field()
    roomtype = scrapy.Field()
    age = scrapy.Field()
    elevator = scrapy.Field()
    floor = scrapy.Field()
    size  = scrapy.Field()
    oriented = scrapy.Field()
    price = scrapy.Field()
    unitprice = scrapy.Field()
    decoration = scrapy.Field()
    time = scrapy.Field()
    datasource = scrapy.Field()
##########
    # blockurl = scrapy.Field()
    # test = scrapy.Field()
    # blockname = scrapy.Field()
    houseinfo = scrapy.Field()
    # data = scrapy.Field()
'''
    title
    longitude
    latitude
    province
    city
    district
    street
    roomtype
    age
    elevator
    floor
    size=
    oriented
    price
    unitprice
    decoration
    time
    datasource

[description]
'''


